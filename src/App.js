import React from 'react';
import './stylesheets/ress.css';
import {Header} from './component/Header';
import {Home} from './pages/Home';
import {About} from "./pages/About";
import {Skill} from "./pages/Skill";
import {Product} from "./pages/Product";
import {Blog} from "./pages/Blog";
import {Contact} from "./pages/Contact";
import styled from 'styled-components'
import {BrowserRouter, Route} from "react-router-dom";

const BoxFull = styled.div`
  position: relative;
  height: 100vh;
  width: 100vw;
`;

// const TimeLine = styled.div`
//   position: absolute;
//   bottom: 5%;
//   animation: 10s linear 1s slidein;
//   background: #FFF;
//   height: 3px;
//   @keyframes slidein {
//   from {
//    width: 0;
//    }
//   to   {
//    width: 100%;
//    }
// }
// `;

export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 'Home',
        };
        this.handleAbout = this.handleAbout.bind(this);
    }

    handleAbout() {
        // alert(this.state.page);
    }

    render() {
        return (
            <BrowserRouter>
                <BoxFull>
                    <Header/>
                    <Route exact path="/" component={Home}/>
                    <Route path="/about" component={About}/>
                    <Route path="/skill" component={Skill}/>
                    <Route path="/product" component={Product}/>
                    <Route path="/blog" component={Blog}/>
                    <Route path="/contact" component={Contact}/>
                    {/*<TimeLine/>*/}
                </BoxFull>
            </BrowserRouter>
        );
    }
}

export default App;

