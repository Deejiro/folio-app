import React from 'react';

let result = null;
const timeout = (msec) => new Promise(resolve => {
    setTimeout(resolve, msec)
});

const LazyComponent = () => {
    if (result !== null) {
        return (
            <div>{result}</div>
        )
    }
    throw new Promise(async(resolve) => {
        await timeout(5000);
        result = 'OK';
        resolve();
    })
};

export default LazyComponent;
