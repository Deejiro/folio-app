import React from "react";
import styled from "styled-components";
import product from  '../images/about-baclground.jpg';

const AboutProduct = styled.div`
    background-image: url(${product});
    background-size: cover;
    height: 100%;
    color: #FFF;
}
`;

export class Product extends React.Component {
    render() {
        return (
            <AboutProduct>
                <h2>Producr</h2>
                <p>プロダクトページです</p>
            </AboutProduct>
        );
    }
}
