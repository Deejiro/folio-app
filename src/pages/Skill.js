import React from "react";
import styled from "styled-components";
import skill from  '../images/skill-baclground.jpg';

const AboutSkill = styled.div`
    background-image: url(${skill});
    background-size: cover;
    height: 100%;
    color: #FFF;
`;

export class Skill extends React.Component {
    render() {
        return (
            <AboutSkill>
                <h2>Skills</h2>
                <p>Skillsです</p>
            </AboutSkill>
        );
    }
}
