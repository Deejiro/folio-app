import React from "react";
import styled from "styled-components";
import about from  '../images/about-baclground.jpg';

const AboutBlog = styled.div`
    background-image: url(${about});
    background-size: cover;
    height: 100%;
    color: #FFF;
`;

export class Blog extends React.Component {
    render() {
        return (
            <AboutBlog>
                <h2>Blog</h2>
                <p>フレンズに投票するページです</p>
            </AboutBlog>
        );
    }
}
