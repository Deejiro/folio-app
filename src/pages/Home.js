import React from "react";
import styled from "styled-components";
import HomeImageBaseball from "../images/baseball.jpg";

const HomePage = styled.div`
    background-image: url(${HomeImageBaseball});
    background-size: cover;
    height: 100%;
    color: #FFF;
}
`;

const MainBox = styled.div`
    position: relative;
`;

const Introduction = styled.div`
    position: absolute;
    top: 85vh;
    left: 0;
    right: 0;
`;

const IntroductionText = styled.p`
    font-size: 3rem;
    text-align: center;
    letter-spacing: 3px;
`;

const Hello = () => {
    const str = [
        'H',
        'e',
        'l',
        'l',
        'o',
        '!',
        '!',
        ' ',
        'I',
        ' ',
        'a',
        'm',
        ' ',
        'D',
        'a',
        'i',
        '.',
    ];

    return (
      <Introduction>
          <IntroductionText>{str}</IntroductionText>
      </Introduction>
    );
};


export class Home extends React.Component {
    render() {
        return (
            <HomePage>
                <MainBox>
                    <Hello/>
                </MainBox>
            </HomePage>
        );
    }
}
