import React from "react";
import styled from "styled-components";
import page from  '../images/about-baclground.jpg';

const AboutPage = styled.div`
    background-color: #1a1a1a;
    height: 100%;
    color: #FFF;
`;

const Letter = styled.div`
    padding: 150px 150px 0;
    writing-mode: vertical-rl;
    width: 100%;
    max-height: 650px;
    text-align: justify;
    text-justify: inter-ideograph;
    letter-spacing: 5px;
    font-size: 14px;
    font-weight: 400;
    line-height: 45px;
`;

const LetterText = styled.p`
    writing-mode: vertical-rl;
    padding-left: 50px;
    font-family: han2;
}
`;



export class About extends React.Component {
    render() {
        return (
            <AboutPage>
                <Letter>
                    <LetterText>はじめまして、竹内大二郎と申します。<br />大阪在住の会社員でウェブ系フロントエンジニアを担当しております。</LetterText>
                    <LetterText>会社では主にマークアップを担当しており、業務外ではアプリケーションを自作して日々スキルアップに励んでおります。</LetterText>
                    <LetterText>このサイトの制作、デザインはすべて自作しております。</LetterText>
                    <LetterText>こちらのポートフォリオはシングルページアプリケーションで制作しており、サーバーはクラウドサーバーを利用しております。</LetterText>
                    <LetterText>ポートフォリオを制作した背景として、人との繋がりを大切にしたいといった理由があります。</LetterText>
                    <LetterText>サイトのコンタクトページでソーシャルネットワーキングサービスのアカウントを搭載しておりますので、フォローやご連絡頂けると幸いです。</LetterText>
                </Letter>
            </AboutPage>
        );
    }
}
