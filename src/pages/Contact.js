import React from "react";
import styled from "styled-components";
import contact from  '../images/about-baclground.jpg';

const AboutContact = styled.div`
    background-image: url(${contact});
    background-size: cover;
    height: 100%;
    color: #FFF;
`;

export class Contact extends React.Component {
    render() {
        return (
            <AboutContact>
                <h2>Contact</h2>
                <p>contactページです</p>
            </AboutContact>
        );
    }
}
