import React from "react";
import styled from "styled-components";
import {Link} from "react-router-dom";

const HeaderInner = styled.div`
  position: absolute;
  top: 30px;
  right: 10%;
  font-size: 1.5rem;
  color: #000;
  width: 600px;
`;

const GlobalNaviList = styled.ul`
    display: flex;
    justify-content: space-between;
    width: 100%;
    list-style: none;
`;

const GlobalNaviListItem = styled.li`
    a {
        color: #FFF;
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
    }
`;

export class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 'Home',
        };
    }
    render() {
        return (
            <HeaderInner>
                <GlobalNaviList>
                    <GlobalNaviListItem>
                        <Link to="/">{this.state.page}</Link>
                    </GlobalNaviListItem>
                    <GlobalNaviListItem>
                        <Link to="/about" onClick={this.handleAbout}>About</Link>
                    </GlobalNaviListItem>
                    <GlobalNaviListItem>
                        <Link to="/skill">Skill</Link>
                    </GlobalNaviListItem>
                    <GlobalNaviListItem>
                        <Link to="/product">Product</Link>
                    </GlobalNaviListItem>
                    <GlobalNaviListItem>
                        <Link to="/blog">Blog</Link>
                    </GlobalNaviListItem>
                    <GlobalNaviListItem>
                        <Link to="/contact">Contact</Link>
                    </GlobalNaviListItem>
                </GlobalNaviList>
            </HeaderInner>
        );
    }
}
