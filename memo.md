サイト構成
- SPA
- AWS / Firebase
- Circle CI
- Docker/ docker-compose

- circle CI で firebaseに自動デプロイできる環境を作れる。
- circle CI の実行タイミングはGithubのブランチにマージされたタイミング。
- ブランチ別にデプロイ先を変更することもできる
    - developブランチはここ、masterブランチはここ、みたいに。
    
- Dockerはみんなで開発環境を合わせられる便利ツール。
- 個人開発では全然意味がないけど、複数人開発ではめちゃくちゃ便利。
- 例えばPHPのバージョンを揃えないといけないとか、仮想環境を合わせないといけないとか、そういったときに`$ docker run`で開発環境が揃う手はずが整う。

